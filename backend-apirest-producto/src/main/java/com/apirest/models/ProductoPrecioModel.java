package com.apirest.models;

public class ProductoPrecioModel {

	String id;
	double precio;

	public ProductoPrecioModel() {
	};

	public ProductoPrecioModel(String id, double precio) {
		this.id = id;
		this.precio = precio;
	}

	public String getId() {
		return id;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}