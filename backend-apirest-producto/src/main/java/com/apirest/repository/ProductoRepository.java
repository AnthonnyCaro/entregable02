package com.apirest.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.apirest.models.ProductoModel;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}