package com.apirest.controllers;

import com.apirest.models.ProductoModel;
import com.apirest.services.ProductoDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

	@Autowired
	private ProductoDBService productoDBService;

	@GetMapping("")
	public String root() {
		return "Techu API REST v2.0.0 - Entregable02 - Anthonny Caro";
	}

    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoDBService.findAll();
    }

    //POST para crear un producto
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {
    	productoDBService.save(newProduct);
        return newProduct;
    }

    // GET a un único producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public ProductoModel getProductoById(@PathVariable String id){
       // return productoService.findById(id);
        return productoDBService.myfindById(id);
    }

    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate){
    	productoDBService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete){
        return productoDBService.delete(productoToDelete);
    }
	
	/*
	// GET instancia por id
	@SuppressWarnings("rawtypes")
	@GetMapping("/productos/{id}")
	public ResponseEntity getProductoId(@PathVariable int id) {
		ProductoModel pr = productService.getProducto(id);
		if (pr == null) {
			return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(pr);
	}

	// POST
	@PostMapping("/productos")
	public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
		productService.addProducto(productoModel);
		return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
	}

	// PUT
	@SuppressWarnings("rawtypes")
	@PutMapping("/productos/{id}")
	public ResponseEntity updateProducto(@PathVariable int id, @RequestBody ProductoModel productToUpdate) {
		ProductoModel pr = productService.getProducto(id);
		if (pr == null) {
			return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
		}
		productService.updateProducto(id - 1, productToUpdate);
		return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
	}

	// DELETE
	@SuppressWarnings("rawtypes")
	@DeleteMapping("/productos/{id}")
	public ResponseEntity deleteProducto(@PathVariable Integer id) {
		ProductoModel pr = productService.getProducto(id);
		if (pr == null) {
			return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
		}
		productService.removeProducto(id - 1);
		return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK); // También 204 No Content
	}

	// PATCH
	@SuppressWarnings("rawtypes")
	@PatchMapping("/productos/{id}")
	public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecioOnly productoPrecioOnly,
			@PathVariable int id) {
		ProductoModel pr = productService.getProducto(id);
		if (pr == null) {
			return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
		}
		pr.setPrecio(productoPrecioOnly.getPrecio());
		productService.updateProducto(id - 1, pr);
		return new ResponseEntity<>(pr, HttpStatus.OK);
	}

	// GET subrecurso
	@SuppressWarnings("rawtypes")
	@GetMapping("/productos/{id}/users")
	public ResponseEntity getProductIdUsers(@PathVariable int id) {
		ProductoModel pr = productService.getProducto(id);
		if (pr == null) {
			return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
		}
		if (pr.getUsers() != null)
			return ResponseEntity.ok(pr.getUsers());
		else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	*/
	
}