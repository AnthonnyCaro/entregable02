package com.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.apirest.models.ProductoModel;
import com.apirest.repository.ProductoRepository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class ProductoDBService {

    @Autowired
    ProductoRepository productoRepository;
    @Autowired
    MongoOperations mo;

    // READ
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // CREATE
    public ProductoModel save(ProductoModel newProducto){
        return productoRepository.save(newProducto);
    }

    // READ ID
    public Optional<ProductoModel> findById(String id){
       return productoRepository.findById(id);
    }

    // READ ID
    public ProductoModel myfindById(String id){
        return mo.findOne(new Query(Criteria.where("_id").is(id)), ProductoModel.class);
    }

    // DELETE
    public boolean delete(ProductoModel productoModel) {
        try {
            productoRepository.delete(productoModel);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
