package com.apirest.services;

import com.apirest.models.ProductoModel;
import com.apirest.models.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    private List<ProductoModel> dataList = new ArrayList<ProductoModel>();

    public ProductoService() {
        dataList.add(new ProductoModel(1, "Producto 001", 120.50));
        dataList.add(new ProductoModel(2, "Producto 002", 130.00));
        dataList.add(new ProductoModel(3, "Producto 003", 110.00));
        dataList.add(new ProductoModel(4, "Producto 004", 50.75));
        dataList.add(new ProductoModel(5, "Producto 005", 115.00));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("2"));
        users.add(new UserModel("7"));
        dataList.get(1).setUsers(users);
    }

    // READ Collections
    public List<ProductoModel> getProductos() {
        return dataList;
    }

    // READ instance
     public ProductoModel getProducto(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
     }

    // CREATE
    public ProductoModel addProducto(ProductoModel newPro) {
        dataList.add(newPro);
        return newPro;
    }

    // UPDATE
    public ProductoModel updateProducto(int index, ProductoModel newPro)
                        throws IndexOutOfBoundsException {
        dataList.set(index, newPro);
        return dataList.get(index);
    }

    // DELETE
    public void removeProducto(int index) throws IndexOutOfBoundsException {
        int pos = dataList.indexOf(dataList.get(index));
        dataList.remove(pos);
    }

    // Get index
    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }

}